# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_user_telephone'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_ens',
            field=models.BooleanField(default=True),
        ),
    ]
