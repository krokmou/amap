# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0007_auto_20160930_2259'),
    ]

    operations = [
        migrations.AddField(
            model_name='commande',
            name='coef_panier',
            field=models.DecimalField(null=True, decimal_places=1, max_digits=1, blank=True),
        ),
        migrations.AddField(
            model_name='panier',
            name='coef_panier',
            field=models.DecimalField(null=True, decimal_places=1, max_digits=1, blank=True),
        ),
    ]
