# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0002_auto_20160904_1526'),
    ]

    operations = [
        migrations.AlterField(
            model_name='credit',
            name='validite',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='livraison',
            name='date',
            field=models.DateTimeField(help_text='%d/%m/%y %H:%M:%S'),
        ),
    ]
