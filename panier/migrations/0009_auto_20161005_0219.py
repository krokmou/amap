# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0008_auto_20161005_0215'),
    ]

    operations = [
        migrations.RenameField(
            model_name='commande',
            old_name='coef_panier',
            new_name='coefficient_panier',
        ),
        migrations.RenameField(
            model_name='panier',
            old_name='coef_panier',
            new_name='coefficient_panier',
        ),
    ]
