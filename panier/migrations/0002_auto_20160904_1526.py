# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='livraison',
            name='date',
            field=models.DateTimeField(unique=True),
        ),
        migrations.AlterField(
            model_name='paiement',
            name='moyen',
            field=models.CharField(unique=True, max_length=255),
        ),
    ]
