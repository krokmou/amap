# coding: utf8
# App de gestion des paniers pour Amap
# Gabriel Détraz
# Gplv2

from django.shortcuts import render_to_response, get_object_or_404, render, redirect
from django.template.context_processors import csrf
from django.template import Context, RequestContext, loader
from django.contrib import messages
from django.db import IntegrityError
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect

from django.db import transaction
from reversion import revisions as reversion
from .models import Panier, Paiement, Commande, Credit, Livraison
from .forms import PaiementForm, CreditForm, CreditFullForm, CreditNoteForm, DelPaiementForm, LivraisonForm, SelectOneLivraisonForm, SelectLivraisonForm
from .forms import PanierForm, DelPanierForm, SelectArticleForm, FullEditCommandeForm, BaseEditCommandeForm, NewCommandeForm
from django.forms import modelformset_factory, formset_factory
from reversion.models import Version
from fractions import Fraction

from amap.settings import note_server, note_port, note_id

from users.views import User
from django.utils import timezone

from amap.settings import MINIMUM_LEVEL, PAGINATION_LARGE_NUMBER, PAGINATION_NUMBER

from .note import connect, login, don

def form(ctx, template, request):
    c = ctx
    c.update(csrf(request))
    return render(
        request,
        template,
        c
    )

@login_required
def new_commande(request, userid):
    try:
        user = User.objects.get(pk=userid)
    except User.DoesNotExist:
        messages.error(request, u"Utilisateur inexistant" )
        return redirect("/panier/")
    if not request.user.has_perms(('bureau',)) and user != request.user:
        messages.error(request, "Vous ne pouvez pas commander pour quelqu'un d'autre !")
        return redirect("/users/profil/" + str(request.user.id))
    commande = Commande(user=user)
    commande_form = NewCommandeForm(request.POST or None, instance=commande, is_bureau=request.user.has_perms(('bureau',)))
    article_form = SelectArticleForm(request.POST or None)
    if commande_form.is_valid() and article_form.is_valid():
        commande = commande_form.save(commit=False)
        commande.prix_unitaire = article_form.cleaned_data['article'].prix
        commande.nom = article_form.cleaned_data['article'].nom
        commande.coefficient_panier = article_form.cleaned_data['article'].coefficient_panier
        # Si non surdroits, check solde et date
        if not request.user.has_perms(('bureau',)):
            if timezone.now() > commande.date.date_modif:
                messages.error(request, "Vous ne pouvez plus passer commande, livraison proche")
                return redirect("/users/profil/" + str(request.user.id))
            if user.solde() - commande.prix() < MINIMUM_LEVEL:
                messages.error(request, "Solde insuffisant pour effectuer l'operation")
                return redirect("/users/profil/" + userid)
        with transaction.atomic(), reversion.create_revision():
            commande.save()
            reversion.set_user(request.user)
            reversion.set_comment("Création")
        messages.success(request, "La commande est enregistree")
        return redirect("/users/profil/" + userid)
    return form({'article_form': article_form, 'commande_form': commande_form}, 'panier/new_commande.html', request)

@login_required
def edit_commande(request, commandeid):
    try:
        commande_instance = Commande.objects.get(pk=commandeid)
    except Commande.DoesNotExist:
        messages.error(request, u"Entrée inexistante" )
        return redirect("/panier/")
    if not request.user.has_perms(('bureau',)) and commande_instance.user != request.user:
        messages.error(request, "Vous ne pouvez pas commander pour quelqu'un d'autre !")
        return redirect("/users/profil/" + str(request.user.id))
    if request.user.has_perms(('bureau',)):
        commande = FullEditCommandeForm(request.POST or None, instance=commande_instance)
    else:
        if timezone.now() > commande_instance.date.date_modif:
            messages.error(request, "Vous ne pouvez plus modifier la commande, livraison proche")
            return redirect("/users/profil/" + str(request.user.id))
        commande = BaseEditCommandeForm(request.POST or None, instance=commande_instance)
    if commande.is_valid():
        new_commande = commande.save(commit=False)
        if commande_instance.user.solde() - new_commande.prix() < MINIMUM_LEVEL and not request.user.has_perms(('bureau',)):
            messages.error(request, "Solde insuffisant pour effectuer l'operation")
            return redirect("/panier/")
        with transaction.atomic(), reversion.create_revision():
            commande.save()
            reversion.set_user(request.user)
            reversion.set_comment("Champs modifié(s) : %s" % ', '.join(field for field in commande.changed_data))
        messages.success(request, "Commande modifiée")
        #return redirect("/panier/")
        return HttpResponseRedirect("")
    return form({'panierform': commande}, 'panier/panier.html', request)

@login_required
def del_commande(request, commandeid):
    try:
        commande = Commande.objects.get(pk=commandeid)
    except Commande.DoesNotExist:
        messages.error(request, u"Credit inexistant" )
        return redirect("/panier/")
    if not request.user.has_perms(('bureau',)) and commande.user != request.user:
        messages.error(request, "Vous ne pouvez pas commander pour quelqu'un d'autre !")
        return redirect("/users/profil/" + str(request.user.id))
    if timezone.now() > commande.date.date_modif and not request.user.has_perms(('bureau',)):
        messages.error(request, "Vous ne pouvez plus supprimer la commande, livraison proche")
        return redirect("/users/profil/" + str(request.user.id))
    if request.method == "POST":
        with transaction.atomic(), reversion.create_revision():
            commande.delete()
            reversion.set_user(request.user)
        messages.success(request, "La commande a été détruite")
        return redirect("/panier/")
    return form({'objet': commande, 'objet_name': 'commande'}, 'panier/delete.html', request)

@login_required
@permission_required('bureau')
def add_livraison(request):
    livraison = LivraisonForm(request.POST or None)
    if livraison.is_valid():
        with transaction.atomic(), reversion.create_revision():
            livraison.save()
            reversion.set_user(request.user)
            reversion.set_comment("Creation")
        messages.success(request, "La date de livraison a ete ajoutee")
        return redirect("/panier/index_livraison/")
    return form({'panierform': livraison}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def edit_livraison(request, livraisonid):
    try:
        livraison_instance = Livraison.objects.get(pk=livraisonid)
    except Livraison.DoesNotExist:
        messages.error(request, u"Entrée inexistante" )
        return redirect("/panier/")
    livraison = LivraisonForm(request.POST or None, instance=livraison_instance)
    if livraison.is_valid():
        with transaction.atomic(), reversion.create_revision():
            livraison.save()
            reversion.set_user(request.user)
            reversion.set_comment("Champs modifié(s) : %s" % ', '.join(field for field in livraison.changed_data))
        messages.success(request, "Date modifiee")
        return redirect("/panier/index_livraison/")
    return form({'panierform': livraison}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def del_livraison(request):
    livraison = SelectLivraisonForm(request.POST or None)
    if livraison.is_valid():
        livraison_dels = livraison.cleaned_data['date']
        for livraison_del in livraison_dels:
            try:
                with transaction.atomic(), reversion.create_revision():
                    livraison_del.delete()
                    reversion.set_comment("Destruction")
                messages.success(request, "La date de livraison a ete supprimee")
            except ProtectedError:
                messages.error(
                    request,
                    "La livraison est affectee a au moins une commande, \
                        vous ne pouvez pas le supprimer" % livraison_del)
        return redirect("/panier/index_livraison/")
    return form({'panierform': livraison}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def add_paiement(request):
    paiement = PaiementForm(request.POST or None)
    if paiement.is_valid():
        with transaction.atomic(), reversion.create_revision():
            paiement.save()
            reversion.set_user(request.user)
            reversion.set_comment("Creation")
        messages.success(request, "Le moyen de paiement a ete ajoute")
        return redirect("/panier/index_paiement/")
    return form({'panierform': paiement}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def edit_paiement(request, paiementid):
    try:
        paiement_instance = Paiement.objects.get(pk=paiementid)
    except Paiement.DoesNotExist:
        messages.error(request, "Entrée inexistante" )
        return redirect("/panier/index_paiement/")
    paiement = PaiementForm(request.POST or None, instance=paiement_instance)
    if paiement.is_valid():
        with transaction.atomic(), reversion.create_revision():
            paiement.save()
            reversion.set_user(request.user)
            reversion.set_comment("Champs modifié(s) : %s" % ', '.join(field for field in paiement.changed_data))
        messages.success(request, "Type de paiement modifié")
        return redirect("/panier/index_paiement/")
    return form({'panierform': paiement}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def del_paiement(request):
    paiement = DelPaiementForm(request.POST or None)
    if paiement.is_valid():
        paiement_dels = paiement.cleaned_data['paiements']
        for paiement_del in paiement_dels:
            try:
                with transaction.atomic(), reversion.create_revision():
                    paiement_del.delete()
                    reversion.set_user(request.user)
                    reversion.set_comment("Destruction")
                messages.success(request, "Le moyen de paiement a été supprimé")
            except ProtectedError:
                messages.error(request, "Le moyen de paiement %s est affecté à au moins une facture, vous ne pouvez pas le supprimer" % paiement_del)
        return redirect("/panier/index_paiement/")
    return form({'panierform': paiement}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def add_credit(request, userid):
    try:
        user = User.objects.get(pk=userid)
    except User.DoesNotExist:
        messages.error(request, "Utilisateur inexistant")
        return redirect("/panier/")
    credit = CreditForm(request.POST or None)
    if credit.is_valid():
        credit = credit.save(commit=False)
        credit.user = user
        with transaction.atomic(), reversion.create_revision():
            credit.save()
            reversion.set_user(request.user)
            reversion.set_comment("Creation")
        messages.success(request, "Le credit a ete ajoute")
        return redirect("/users/profil/" + userid)
    return form({'panierform': credit}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def edit_credit(request, creditid):
    try:
        credit_instance = Credit.objects.get(pk=creditid)
    except Credit.DoesNotExist:
        messages.error(request, u"Entrée inexistante" )
        return redirect("/panier/")
    credit = CreditFullForm(request.POST or None, instance=credit_instance)
    if credit.is_valid():
        with transaction.atomic(), reversion.create_revision():
            credit.save()
            reversion.set_user(request.user)
            reversion.set_comment("Champs modifié(s) : %s" % ', '.join(field for field in credit.changed_data))
        messages.success(request, "Date modifiee")
        return redirect("/panier/")
    return form({'panierform': credit}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def del_credit(request, creditid):
    try:
        credit = Credit.objects.get(pk=creditid)
    except Credit.DoesNotExist:
        messages.error(request, u"Credit inexistant" )
        return redirect("/panier/")
    if request.method == "POST":
        with transaction.atomic(), reversion.create_revision():
            credit.delete()
            reversion.set_user(request.user)
        messages.success(request, "Le credit a été détrite")
        return redirect("/panier/")
    return form({'objet': credit, 'objet_name': 'credit'}, 'panier/delete.html', request)

@login_required
def add_credit_note(request, userid):
    """
    Build a request to start the negociation with NoteKfet
    """
    try:
        user = User.objects.get(pk=userid)
    except User.DoesNotExist:
        messages.error(request, "Utilisateur inexistant")
        return redirect("/panier/")
    noteform = CreditNoteForm(request.POST or None)
    if noteform.is_valid():
        pseudo = noteform.cleaned_data['login']
        password = noteform.cleaned_data['password']
        montant = noteform.cleaned_data['montant']
        result, sock, err = login(note_server, note_port, pseudo, password)
        if not result:
            messages.error(request, err)
            return redirect("/users/profil/" + userid)
        else:
            result, err = don(sock, montant, note_id)
            if not result:
                messages.error(request, err)
                return redirect("/users/profil/" + userid)
        moyen = Paiement.objects.filter(moyen='Note Kfet (self)').get()
        moyen.save()
        credit = Credit(user=user,montant=montant,moyen=moyen,validite=True)
        with transaction.atomic(), reversion.create_revision():
            credit.save()
            reversion.set_user(request.user)
            reversion.set_comment("Creation")
        messages.success(request, "Le paiement par note a bien été effectué")
        return redirect("/users/profil/" + userid)
    return form({'panierform': noteform}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def add_article(request):
    article = PanierForm(request.POST or None)
    if article.is_valid():
        with transaction.atomic(), reversion.create_revision():
            article.save()
            reversion.set_user(request.user)
            reversion.set_comment("Création")
        messages.success(request, "L'article a été ajouté")
        return redirect("/panier/index_article/")
    return form({'panierform': article}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def edit_article(request, articleid):
    try:
        article_instance = Panier.objects.get(pk=articleid)
    except Panier.DoesNotExist:
        messages.error(request, u"Entrée inexistante" )
        return redirect("/panier/index_article/")
    article = PanierForm(request.POST or None, instance=article_instance)
    if article.is_valid():
        with transaction.atomic(), reversion.create_revision():
            article.save()
            reversion.set_user(request.user)
            reversion.set_comment("Champs modifié(s) : %s" % ', '.join(field for field in article.changed_data))
        messages.success(request, "Type d'article modifié")
        return redirect("/panier/index_article/")
    return form({'panierform': article}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def del_article(request):
    article = DelPanierForm(request.POST or None)
    if article.is_valid():
        article_del = article.cleaned_data['paniers']
        with transaction.atomic(), reversion.create_revision():
            article_del.delete()
            reversion.set_user(request.user)
        messages.success(request, "Le/les articles ont été supprimé")
        return redirect("/panier/index_article")
    return form({'panierform': article}, 'panier/panier.html', request)

@login_required
@permission_required('bureau')
def control(request, livraisonid=False):
    if not livraisonid:
        livraison = SelectOneLivraisonForm(request.POST or None)
        if livraison.is_valid():
            return redirect("/panier/control/" + str(livraison.cleaned_data['date'].id))
        return render(request, 'panier/control.html', {'livraisonform': livraison})
    else:
        livraison_select = Livraison.objects.get(id=livraisonid)
        commande_list = Commande.objects.filter(date=livraison_select).order_by('date')
        controlform_set = modelformset_factory(Commande, fields=('livre',), extra=0)
        controlform = controlform_set(request.POST or None, queryset=commande_list)
        if controlform.is_valid():
            with transaction.atomic(), reversion.create_revision():
                controlform.save()
                reversion.set_user(request.user)
                reversion.set_comment("Etat de livraison")
            messages.success(request, "Livraisons enregistrees")
            return redirect("/panier/control/")
        livraison_commandes = len(Commande.objects.filter(date=Livraison.objects.filter(date=livraison_select.date)))
        livraison_paniers = sum(pan.coefficient_panier*pan.quantite for pan in Commande.objects.filter(date=Livraison.objects.filter(date=livraison_select.date)))
        livraison_quantite={}
        for coeff in list(Commande.objects.all().values_list('coefficient_panier', flat=True).distinct()):
            livraison_quantite[Fraction(coeff)]=Commande.objects.filter(date=Livraison.objects.filter(date=livraison_select.date)).filter(livre=False).filter(coefficient_panier=coeff).count()

        return render(request, 'panier/control.html', {'controlform': controlform, 'livraison_commandes': livraison_commandes, 'livraison_paniers': livraison_paniers,'livraison_quantite':livraison_quantite})

@login_required
@permission_required('bureau')
def add_sac(request, userid):
    try:
        user_instance = User.objects.get(pk=userid)
    except User.DoesNotExist:
        messages.error(request, u"User inexistante" )
    try:
        article_instance = Panier.objects.get(nom="Sac")
    except Panier.DoesNotExist:
        messages.error(request, u"Merci de créer une entrée Sac" )
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    with transaction.atomic(), reversion.create_revision():
        user_instance.sac_consignes += 1
        user_instance.save()
        reversion.set_user(request.user)
        reversion.set_comment("Ajout sac")
    messages.success(request, u"Sac ajouté à %s (dispose actuellement de %s sacs)" % (user_instance.get_full_name(), user_instance.sac_consignes) )
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
@permission_required('bureau')
def del_sac(request, userid):
    try:
        user_instance = User.objects.get(pk=userid)
    except User.DoesNotExist:
        messages.error(request, u"User inexistante" )
    try:
        article_instance = Panier.objects.get(nom="Sac")
    except Panier.DoesNotExist:
        messages.error(request, u"Merci de créer une entrée Sac" )
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    if user_instance.sac_consignes > 0:
        with transaction.atomic(), reversion.create_revision():
            user_instance.sac_consignes -= 1
            user_instance.save()
            reversion.set_user(request.user)
            reversion.set_comment("Retire sac")
        messages.success(request, u"Sac retiré à %s (dispose actuellement de %s sacs)" % (user_instance.get_full_name(), user_instance.sac_consignes) )
    else:
        messages.error(request, u"Nombre de sac nul !" )
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required
def history(request, object, id):
    if object == 'credit':
        try:
             object_instance = Credit.objects.get(pk=id)
        except Credit.DoesNotExist:
             messages.error(request, "Credit inexistant")
             return redirect("/panier/")
        if not request.user.has_perms(('bureau',)) and object_instance.user != request.user:
             messages.error(request, "Vous ne pouvez pas afficher l'historique d'une facture d'un autre user que vous sans droit ")
             return redirect("/panier/")
    elif object == 'commande':
        try:
             object_instance = Commande.objects.get(pk=id)
        except Commande.DoesNotExist:
             messages.error(request, "Commande inexistante")
             return redirect("/panier/")
        if not request.user.has_perms(('bureau',)) and object_instance.user != request.user:
             messages.error(request, "Vous ne pouvez pas afficher l'historique d'une commande d'un autre user que vous sans droit ")
             return redirect("/panier/")
    elif object == 'paiement' and request.user.has_perms(('bureau',)):
        try:
             object_instance = Paiement.objects.get(pk=id)
        except Paiement.DoesNotExist:
             messages.error(request, "Paiement inexistant")
             return redirect("/panier/")
    elif object == 'article' and request.user.has_perms(('bureau',)):
        try:
             object_instance = Panier.objects.get(pk=id)
        except Panier.DoesNotExist:
             messages.error(request, "Paiement inexistant")
             return redirect("/panier/")
    elif object == 'livraison' and request.user.has_perms(('bureau',)):
        try:
             object_instance = Livraison.objects.get(pk=id)
        except Livraison.DoesNotExist:
             messages.error(request, "Livraison inexistante")
             return redirect("/panier/")
    else:
        messages.error(request, "Objet  inconnu")
        return redirect("/panier/")
    reversions = Version.objects.get_for_object(object_instance)
    paginator = Paginator(reversions, PAGINATION_NUMBER)
    page = request.GET.get('page')
    try:
        reversions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        reversions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        reversions = paginator.page(paginator.num_pages)
    return render(request, 'amap/history.html', {'reversions': reversions, 'object': object_instance})


@login_required
def index(request):
    paniers_list = Commande.objects.order_by('date').reverse()
    credit_list = Credit.objects.order_by('pk').reverse()
    if not request.user.has_perms(('bureau',)):
        paniers_list = paniers_list.filter(user=request.user)
        credit_list = credit_list.filter(user=request.user)
    paginator_paniers = Paginator(paniers_list, PAGINATION_LARGE_NUMBER)
    paginator_credit = Paginator(credit_list, PAGINATION_NUMBER)
    page1 = request.GET.get('page1')
    page2 = request.GET.get('page2')
    try:
        paniers_list = paginator_paniers.page(page1)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        paniers_list = paginator_paniers.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        paniers_list = paginator_paniers.page(paginator_paniers.num_pages)
    try:
        credit_list = paginator_credit.page(page2)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        credit_list = paginator_credit.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        credit_list = paginator_credit.page(paginator_credit.num_pages)

    management_view = request.user.has_perms(('bureau',))
    solde_total = sum(us.solde() for us in User.objects.all())
    user_total = len(User.objects.filter(state=0))
    user_ens = len(User.objects.filter(state=0).filter(is_ens=True))
    solde_moyen = round(solde_total/user_total, 2)
    commandes_total = len(Commande.objects.all())
    next_livraison = Livraison.objects.order_by('date').filter(date__gt=timezone.now()).first()
    next_livraison_commandes = 0
    next_livraison_paniers = 0
    if next_livraison:
        next_livraison_commandes = Commande.objects.filter(date=Livraison.objects.filter(date=next_livraison.date)).count()
        next_livraison_paniers = sum(pan.coefficient_panier*pan.quantite for pan in Commande.objects.filter(date=Livraison.objects.filter(date=next_livraison.date)))
    return render(request, 'panier/index.html', {'page1': page1, 'page2': page2, 'paniers_list': paniers_list, 'management_view': management_view, 'credit_list': credit_list, 'solde_total': solde_total, 'user_total': user_total, 'user_ens': user_ens, 'solde_moyen': solde_moyen, 'commandes_total': commandes_total, 'next_livraison': next_livraison, 'next_livraison_commandes': next_livraison_commandes, 'next_livraison_paniers': next_livraison_paniers})

@login_required
def index_article(request):
    article_list = Panier.objects.order_by('nom')
    return render(request, 'panier/index_article.html', {'article_list':article_list})

@login_required
@permission_required('bureau')
def index_paiement(request):
    paiement_list = Paiement.objects.order_by('moyen')
    return render(request, 'panier/index_paiement.html', {'paiement_list':paiement_list})

@login_required
def index_livraison(request):
    livraison_list = Livraison.objects.order_by('date').reverse()
    return render(request, 'panier/index_livraison.html', {'livraison_list':livraison_list})

@login_required
def plusProcheLivraison(request):
    liste_livraisons = Livraison.objects.all()
    liste_delta=[abs(timezone.now()-livraison.date) for livraison in liste_livraisons]
    proche=liste_livraisons[liste_delta.index(min(liste_delta))]
    return redirect("/panier/control/"+str(proche.id))
