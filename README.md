# Amap

Basé sur re2o 2016, developpe au rezometz par Goulven Kermarec, Gabriel Détraz et Augustin Lemesle

## Avant propos 

Amap est un logiciel d'administration développé initiallement au rezometz. Il se veut agnostique au réseau considéré, de manière à être installable en quelques clics.

Il utilise le framework django avec python3. Il permet de gérer les commandes et paiements de paniers pour l'amap.
Il s'agit d'un système modulaire, où on peut pluguer facilement de nouvelles fonctionnalitési besoin.

## Installation

Dépendances :

 * python3-django (1.8, jessie-backports)
 * python3-django-reversion (1.10, stretch)
 * django-bootstrap3 (pip3 install)

Moteur de db conseillé (mysql), postgresql fonctionne également.
Pour mysql, il faut installer :

 * mysql-server (jessie)
 * python3-mysqldb (jessie-backports)

## Configuration 

Le site est prêt a fonctionner, il faut simplement créer la base de donnée (par défamap), et régler les variables présentes dans setting_local.py
Un fichier d'exemple est disponible.
Ensuite, effectuer les migrations. Un squelette de base de donnée, via un mysqldump peut être fourni.

## Mise en production avec apache

amap/wsgi.py permet de fonctionner avec apache2 en production

## Fonctionnement applicatif

L'application permet d'enregistrer des paniers, des dates de livraisons, des dates limites de commande.
Une commande n'est possible que si le solde est positif. Une fois la date limite passée , les commandes ne peuvent plus etre modifiées. 
Les droits bureau permettent de gérer les livraisons, commandes etc
